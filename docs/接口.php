1.商家详情
POST:{url}/company/{company_id}
参数{_TOKEN_:''}
例如:
http://qyh.local/company/1
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5}
返回:
{
    "status": 0,
    "return": {
        "id": 1,
        "name": "测试",
        "contact_name": "张明",
        "contact_phone": "13333333333",
        "photo": "https://qyh.local/uploads/images/20180522/45308300_1526949454.png",
        "user_id": 1,
        "parent_industry_id": 1,
        "industry_id": 2,
        "position_x": "",
        "position_y": "",
        "status": 1,
        "province": 14,
        "city": 208,
        "district": 1750,
        "address": "测试",
        "company_info": "测试",
        "is_check": 0,
        "reject_reason": "",
        "license_no": "",
        "license_photo": "/uploads/images/20180522/15763600_1526949465.png",
        "pay_password": "",
        "created_at": "2018-05-22 08:43:52",
        "updated_at": "2018-05-22 08:43:52",
        "details": "<p>测试测试</p>",
        "user_name": "Band",
        "consumption": "10000.00",
        "book_keeping": "20000.00",
        "company_index": "1000.00",
        "parent_industry_name": "美食",
        "industry_name": "火锅"
    },
    "data": {
        "id": 1,
        "name": "测试",
        "contact_name": "张明",
        "contact_phone": "13333333333",
        "photo": "https://qyh.local/uploads/images/20180522/45308300_1526949454.png",
        "user_id": 1,
        "parent_industry_id": 1,
        "industry_id": 2,
        "position_x": "",
        "position_y": "",
        "status": 1,
        "province": 14,
        "city": 208,
        "district": 1750,
        "address": "测试",
        "company_info": "测试",
        "is_check": 0,
        "reject_reason": "",
        "license_no": "",
        "license_photo": "/uploads/images/20180522/15763600_1526949465.png",
        "pay_password": "",
        "created_at": "2018-05-22 08:43:52",
        "updated_at": "2018-05-22 08:43:52",
        "details": "<p>测试测试</p>",
        "user_name": "Band",
        "consumption": "10000.00",
        "book_keeping": "20000.00",
        "company_index": "1000.00",
        "parent_industry_name": "美食",
        "industry_name": "火锅"
    }
}


2.申请额度
POST:{url}/company/{company_id}/apply
参数{_TOKEN_:{moeny},money:{money}}
例如:
http://qyh.local/company/1/apply
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:1000}
返回:
成功:

{
    "status": 0,
    "return": "申请成功!",
    "data": "申请成功!"
}

错误消息:
{
    "status": 1,
    "return": "请填写金额!",
    "data": "请填写金额!"
}

3.记账

POST:{url}/company/{company_id}/book
参数{_TOKEN_:{moeny},money:{money},pay_pass:{pay_pass}}
例如:
http://qyh.local/company/1/book
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:1000,pay_pass:123456}
返回:
成功:

{
    "status": 0,
    "return": "记账成功!",
    "data": "记账成功!"
}

错误消息:
{
    "status": 1,
    "return": "请填写支付密码!",
    "data": "请填写支付密码!"
}

{
    "status": 1,
    "return": "用户未认证!",
    "data": "用户未认证!"
}
...



4.记账借条

POST:{url}/user/{user_id}/iou
参数{_TOKEN_:{moeny},money:{money},pay_pass:{pay_pass}}
例如:
http://qyh.local/user/1/iou
POST:{_TOKEN_:bf3b3d4899e46fe64c3ad9d4572b3427cf96ff52142741cb52e03506e08fdce5,money:1000,pay_pass:123456}
返回:
成功:

{
    "status": 0,
    "return": "借条成功!",
    "data": "借条成功!"
}

错误消息:
{
    "status": 1,
    "return": "未找到用户!",
    "data": "未找到用户!"
}
{
    "status": 1,
    "return": "支付密码错误!",
    "data": "支付密码错误!"
}
...

